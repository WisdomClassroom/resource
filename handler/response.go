/*******************************************************************************
 * Copyright 2020 huanggefan.cn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package handler

const (
	ResponseCodeSuc                  = 0
	ResponseCodeErrorFileTooBig      = 1
	ResponseCodeErrorNotAuth         = 2
	ResponseCodeErrorInvalidFile     = 3
	ResponseCodeErrorSaveFailed      = 4
	ResponseCodeErrorChapterNotExist = 5
	ResponseCodeErrorOtherError      = 6
)

type FileInfoResponse struct {
	FileUUID    string `json:"uuid"`
	FileType    string `json:"type"`
	FileName    string `json:"name"`
	PutTime     int64  `json:"time"`
	PutUserUUID string `json:"user"`
}

type UploadResponse struct {
	StatusCode int              `json:"code"`
	Info       FileInfoResponse `json:"info"`
}

type DeleteResponse struct {
	StatusCode int `json:"code"`
}

type ListResponse struct {
	StatusCode int                 `json:"code"`
	InfoList   []*FileInfoResponse `json:"info"`
}
