module gitee.com/WisdomClassroom/resource

go 1.13

require (
	gitee.com/WisdomClassroom/core v0.6.4
	github.com/jinzhu/gorm v1.9.12
)
