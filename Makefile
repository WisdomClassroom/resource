# Copyright 2020 huanggefan.cn
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.PYONY: all, test, clean, sync

SHELL=/bin/bash

_BUILDTAG = $(shell git describe --always)
_BUILDTIME = $(shell date +%F@%T)
_BUILDGO = $(shell go version)

_LDFLAGS = -ldflags '-X "main.BuildTag=${_BUILDTAG}" -X "main.BuildGo=${_BUILDGO}" -X "main.BuildTime=${_BUILDTIME}"'

all: test
	go build -o service-resource ${_LDFLAGS} .

test: clean
	@go test -cover -covermode=count -coverprofile=coverage.out .
	@go tool cover -func=coverage.out
	#@go tool cover -html=coverage.out

clean:
	@rm -vf coverage.out
	@rm -vf service-resource
	@rm -rvf data

sync:
	go mod tidy
	go mod vendor
