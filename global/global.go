/*******************************************************************************
 * Copyright 2020 huanggefan.cn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package global

import (
	"gitee.com/WisdomClassroom/core"
	"github.com/jinzhu/gorm"
)

var Logger *core.Logger
var DB *gorm.DB
var TokenCert *string
var Dir string

const (
	POSTFormFileName = "name"
	POSTFormFile     = "file"
	POSTFormChapter  = "chapter"
	CookiesToken     = "AuthToken"

	QueryFileUUID    = "file"
	QueryChapterUUID = "chapter"

	PostFormFileUUID    = "file"
)

var FileTypeCodeMap map[int]string
var FileTypeMIMEMap map[int]string

const (
	HTTP_Content_Type        = "Content-Type"
	HTTP_Content_Disposition = "Content-Disposition"
)

func init() {
	FileTypeCodeMap = make(map[int]string)
	FileTypeMIMEMap = make(map[int]string)

	FileTypeCodeMap[core.FileTypeCodeJPG] = "jpg"
	FileTypeCodeMap[core.FileTypeCodePNG] = "png"
	FileTypeCodeMap[core.FileTypeCodePPT] = "ppt"
	FileTypeCodeMap[core.FileTypeCodeXLS] = "xls"
	FileTypeCodeMap[core.FileTypeCodeDOC] = "doc"
	FileTypeCodeMap[core.FileTypeCodePPTX] = "pptx"
	FileTypeCodeMap[core.FileTypeCodeXLSX] = "xlsx"
	FileTypeCodeMap[core.FileTypeCodeDOCX] = "docx"
	FileTypeCodeMap[core.FileTypeCodePDF] = "pdf"
	FileTypeCodeMap[core.FileTypeCodeMP3] = "mp3"
	FileTypeCodeMap[core.FileTypeCodeMP4] = "mp4"
	FileTypeCodeMap[core.FileTypeCodeZIP] = "zip"

	FileTypeMIMEMap[core.FileTypeCodeJPG] = "image/jpeg"
	FileTypeMIMEMap[core.FileTypeCodePNG] = "image/png"
	FileTypeMIMEMap[core.FileTypeCodePPT] = "application/vnd.ms-powerpoint"
	FileTypeMIMEMap[core.FileTypeCodeXLS] = "application/vnd.ms-excel"
	FileTypeMIMEMap[core.FileTypeCodeDOC] = "application/msword"
	FileTypeMIMEMap[core.FileTypeCodePPTX] = "application/vnd.ms-powerpoint"
	FileTypeMIMEMap[core.FileTypeCodeXLSX] = "application/vnd.ms-excel"
	FileTypeMIMEMap[core.FileTypeCodeDOCX] = "application/msword"
	FileTypeMIMEMap[core.FileTypeCodePDF] = "application/pdf"
	FileTypeMIMEMap[core.FileTypeCodeMP3] = "audio/mpeg"
	FileTypeMIMEMap[core.FileTypeCodeMP4] = "video/mp4"
	FileTypeMIMEMap[core.FileTypeCodeZIP] = "application/octet-stream"
}
